import logo from '../../logo.svg';
import './index.scss';

const callAPI = () => {
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "http://localhost:8080/of-payment-manager/v1/cancel/payment", true);
  xhr.setRequestHeader('Content-Type', 'application/json');

  xhr.send()
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <button onClick={()=>callAPI()}>Teste API</button>
      </header>
    </div>
  );
}

export default App;
