import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/app';

ReactDOM.render(
  <React.StrictMode>
    // adicionar o theme aqui
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

