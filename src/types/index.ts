export interface BooksType {
  id: string;
  title?: string;
  author?: string;
  amountOfPages?: number;
  currentlyReading?: boolean;
  amazonLink?: string;
  avatarLink?: string;
  sumary: string;
  publishedAt: Date;
  amountOfReviews?: number;
  amountOfRatings?: number; //notas, estrelas
}